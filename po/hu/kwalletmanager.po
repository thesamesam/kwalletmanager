#
# Tamas Szanto <tszanto@interware.hu>, 2003.
# Tamas Krutki <negusnyul@gmail.com>, 2011.
# Balázs Úr <urbalazs@gmail.com>, 2013.
# Kristóf Kiszel <ulysses@kubuntu.org>, 2015.
# SPDX-FileCopyrightText: 2021, 2024 Kristof Kiszel <kiszel.kristof@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: KDE 4.3\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-14 01:37+0000\n"
"PO-Revision-Date: 2024-01-03 10:49+0100\n"
"Last-Translator: Kristof Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kiszel Kristóf"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ulysses@fsf.hu"

#: allyourbase.cpp:209
#, kde-format
msgid "An entry by the name '%1' already exists. Would you like to continue?"
msgstr "Már létezik '%1' nevű bejegyzés. Tovább szeretne lépni?"

#: allyourbase.cpp:234
#, kde-format
msgid "A folder by the name '%1' already exists.  What would you like to do?"
msgstr "Már létezik '%1' nevű mappa. Mit szeretne tenni?"

#: allyourbase.cpp:237
#, kde-format
msgid "Replace"
msgstr "Csere"

#: allyourbase.cpp:347
#, kde-format
msgid "Folders"
msgstr "Könyvtárak"

#: allyourbase.cpp:381
#, kde-format
msgid "An unexpected error occurred trying to drop the item"
msgstr "Nem várt hiba történt elem ejtésekor"

#: allyourbase.cpp:457
#, kde-format
msgid "An unexpected error occurred trying to drop the entry"
msgstr "Nem várt hiba történt a bejegyzés ejtésekor"

#: allyourbase.cpp:481
#, kde-format
msgid ""
"An unexpected error occurred trying to delete the original folder, but the "
"folder has been copied successfully"
msgstr ""
"Nem várt hiba történt az eredeti könyvtár törlésekor, de a könyvtár "
"átmásolása sikeresen megtörtént"

#. i18n: ectx: property (text), widget (QLabel, label)
#: applicationsmanager.ui:17
#, kde-format
msgid "These applications are currently connected to this wallet:"
msgstr "Ezek az alkalmazások kapcsolódnak jelenleg ehhez a tárolóhoz:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: applicationsmanager.ui:64
#, kde-format
msgid "These applications are authorized to access this wallet:"
msgstr ""
"Ezek az alkalmazások vannak felhatalmazva ehhez a tárolóhoz való "
"hozzáférésre:"

#: disconnectappbutton.cpp:16
#, kde-format
msgid "Disconnect"
msgstr "Leválasztás"

#. i18n: ectx: property (text), widget (QPushButton, _allowOnce)
#: kbetterthankdialogbase.ui:43
#, kde-format
msgid "&Replace"
msgstr "&Csere"

#. i18n: ectx: property (text), widget (QPushButton, _allowAlways)
#: kbetterthankdialogbase.ui:53
#, kde-format
msgid "Replace &All"
msgstr "Az összes cs&eréje"

#. i18n: ectx: property (text), widget (QPushButton, _deny)
#: kbetterthankdialogbase.ui:60
#, kde-format
msgid "&Skip"
msgstr "Ki&hagyás"

#. i18n: ectx: property (text), widget (QPushButton, _denyForever)
#: kbetterthankdialogbase.ui:67
#, kde-format
msgid "Skip A&ll"
msgstr "Az összes k&ihagyása"

#: kwalleteditor.cpp:97
#, kde-format
msgid "Search"
msgstr "Keresés"

#: kwalleteditor.cpp:107
#, kde-format
msgid "&Show values"
msgstr "Az ér&tékek megjelenítése"

#: kwalleteditor.cpp:207
#, kde-format
msgid "&New Folder..."
msgstr "Ú&j mappa..."

#: kwalleteditor.cpp:211
#, kde-format
msgid "&Delete Folder"
msgstr "A mappa tör&lése"

#: kwalleteditor.cpp:214
#, kde-format
msgid "&Import a wallet..."
msgstr "Tároló &importálása…"

#: kwalleteditor.cpp:218
#, kde-format
msgid "&Import XML..."
msgstr "XML-adatok &importálása..."

#: kwalleteditor.cpp:222
#, kde-format
msgid "&Export as XML..."
msgstr "&Exportálás XML-ként…"

#: kwalleteditor.cpp:226
#, kde-format
msgid "&Copy"
msgstr "&Másolás"

#: kwalleteditor.cpp:231
#, kde-format
msgid "&New..."
msgstr "Ú&j..."

#: kwalleteditor.cpp:236
#, kde-format
msgid "&Rename"
msgstr "Átne&vezés"

#: kwalleteditor.cpp:241 kwalletpopup.cpp:63
#, kde-format
msgid "&Delete"
msgstr "&Törlés"

#: kwalleteditor.cpp:246
#, kde-format
msgid "Always show contents"
msgstr "Mindig jelenítse meg a tartalmat"

#: kwalleteditor.cpp:250
#, kde-format
msgid "Always hide contents"
msgstr "Mindig rejtse el a tartalmat"

#: kwalleteditor.cpp:359
#, kde-format
msgid "Passwords"
msgstr "Jelszavak"

#: kwalleteditor.cpp:360
#, kde-format
msgid "Maps"
msgstr "Térképek"

#: kwalleteditor.cpp:361
#, kde-format
msgid "Binary Data"
msgstr "Bináris adatok"

#: kwalleteditor.cpp:362
#, kde-format
msgid "Unknown"
msgstr "Ismeretlen"

#: kwalleteditor.cpp:402
#, kde-format
msgid "Are you sure you wish to delete the folder '%1' from the wallet?"
msgstr "Biztosan törölni szeretné a tárolóból ezt a mappát: \"%1\" ?"

#: kwalleteditor.cpp:406
#, kde-format
msgid "Error deleting folder."
msgstr "Egy mappát nem sikerült törölni."

#: kwalleteditor.cpp:425
#, kde-format
msgid "New Folder"
msgstr "Új mappa"

#: kwalleteditor.cpp:426
#, kde-format
msgid "Please choose a name for the new folder:"
msgstr "Adja meg az új mappa nevét:"

#: kwalleteditor.cpp:436
#, kde-format
msgid "Sorry, that folder name is in use. Try again?"
msgstr "Ez a mappanév már foglalt. Megpróbálja még egyszer?"

#: kwalleteditor.cpp:438 kwalleteditor.cpp:803
#, kde-format
msgid "Try Again"
msgstr "Újra"

#: kwalleteditor.cpp:439 kwalleteditor.cpp:804 kwalletmanager.cpp:358
#, kde-format
msgid "Do Not Try"
msgstr "Nem kell újból próbálni"

#: kwalleteditor.cpp:478
#, kde-format
msgid "Error saving entry. Error code: %1"
msgstr "A bejegyzés mentése nem sikerült. Hibakód: %1"

#: kwalleteditor.cpp:500
#, kde-format
msgid ""
"The contents of the current item has changed.\n"
"Do you want to save changes?"
msgstr ""
"A jelenlegi elem tartalma megváltozott.\n"
"Szeretné menteni a változásokat?"

#: kwalleteditor.cpp:554 kwalleteditor.cpp:886
#, kde-format
msgid "Password: %1"
msgstr "Jelszó: %1"

#: kwalleteditor.cpp:571 kwalleteditor.cpp:888
#, kde-format
msgid "Name-Value Map: %1"
msgstr "Név-érték párok: %1"

#: kwalleteditor.cpp:581 kwalleteditor.cpp:890
#, kde-format
msgid "Binary Data: %1"
msgstr "Bináris adatok: %1"

#: kwalleteditor.cpp:789
#, kde-format
msgid "New Entry"
msgstr "Új bejegyzés"

#: kwalleteditor.cpp:790
#, kde-format
msgid "Please choose a name for the new entry:"
msgstr "Adja meg az új bejegyzés nevét:"

#: kwalleteditor.cpp:801
#, kde-format
msgid "Sorry, that entry already exists. Try again?"
msgstr "Ilyen nevű bejegyzés már létezik. Megpróbálja még egyszer?"

#: kwalleteditor.cpp:821 kwalleteditor.cpp:830
#, kde-format
msgid "An unexpected error occurred trying to add the new entry"
msgstr "Nem várt hiba történt új bejegyzés felvételekor"

#: kwalleteditor.cpp:882
#, kde-format
msgid "An unexpected error occurred trying to rename the entry"
msgstr "Nem várt hiba történt a bejegyzés átnevezésekor"

#: kwalleteditor.cpp:902
#, kde-format
msgid "Are you sure you wish to delete the item '%1'?"
msgstr "Biztosan törölni szeretné ezt az elemet: '%1'?"

#: kwalleteditor.cpp:906
#, kde-format
msgid "An unexpected error occurred trying to delete the entry"
msgstr "Nem várt hiba történt a bejegyzés törlésekor"

#: kwalleteditor.cpp:934
#, kde-format
msgid "Unable to open the requested wallet."
msgstr "Nem sikerült megnyitni a kért tárolót."

#: kwalleteditor.cpp:970
#, kde-format
msgid "Unable to create temporary file for downloading '<b>%1</b>'."
msgstr "Nem lehet létrehozni átmeneti fájlt a(z) „<b>%1</b>” letöltéséhez."

#: kwalleteditor.cpp:977
#, kde-format
msgid "Unable to access wallet '<b>%1</b>'."
msgstr "Nem sikerült elérni ezt a tárolót: \"<b>%1</b>\"."

#: kwalleteditor.cpp:1008 kwalleteditor.cpp:1038 kwalleteditor.cpp:1072
#: kwalleteditor.cpp:1154
#, kde-format
msgid ""
"Folder '<b>%1</b>' already contains an entry '<b>%2</b>'.  Do you wish to "
"replace it?"
msgstr ""
"A(z) '<b>%1</b>' mappa már tartalmaz '<b>%2</b>' nevű bejegyzést. Felül "
"szeretné írni a meglevőt?"

#: kwalleteditor.cpp:1112
#, kde-format
msgid "Unable to access XML file '<b>%1</b>'."
msgstr "Nem sikerült elérni a(z) '<b>%1</b>' XML-fájlt."

#: kwalleteditor.cpp:1118
#, kde-format
msgid "Error reading XML file '<b>%1</b>' for input."
msgstr "Nem sikerült olvasni a(z) '<b>%1</b>' XML-fájlból."

#: kwalleteditor.cpp:1124
#, kde-format
msgid "Error: XML file does not contain a wallet."
msgstr "Hiba: az XML-fájl nem tartalmaz jelszótárolót."

#: kwalleteditor.cpp:1275
#, kde-format
msgid "Unable to store to '<b>%1</b>'."
msgstr "Nem lehet tárolni ezt: „<b>%1</b>”."

#: kwalletmanager.cpp:64
#, kde-format
msgid ""
"The KDE Wallet system is not enabled. Do you want me to enable it? If not, "
"the KWalletManager will quit as it cannot work without reading the wallets."
msgstr ""
"A KDE jelszókezelő le van tiltva. Szeretné bekapcsolni? Ha nem, a "
"KWalletManager kilép, mert a jelszótárolók olvasása nélkül nem tud dolgozni."

#: kwalletmanager.cpp:88 kwalletmanager.cpp:95 kwalletmanager.cpp:223
#: kwalletmanager.cpp:288
#, kde-format
msgid "Wallet"
msgstr "Tároló"

#: kwalletmanager.cpp:88 kwalletmanager.cpp:288
#, kde-format
msgid "No wallets open."
msgstr "Nincs megnyitva jelszótároló."

#: kwalletmanager.cpp:95 kwalletmanager.cpp:223
#, kde-format
msgid "A wallet is open."
msgstr "Meg van nyitva egy jelszótároló."

#: kwalletmanager.cpp:140 kwalletpopup.cpp:27
#, kde-format
msgid "&New Wallet..."
msgstr "Ú&j tároló..."

#: kwalletmanager.cpp:145
#, kde-format
msgid "&Delete Wallet..."
msgstr "&Tároló törlése…"

#: kwalletmanager.cpp:150
#, kde-format
msgid "Export as encrypted"
msgstr "Exportálás titkosítva"

#: kwalletmanager.cpp:155
#, kde-format
msgid "&Import encrypted"
msgstr "Titkosított &importálása"

#: kwalletmanager.cpp:160
#, kde-format
msgid "Configure &Wallet..."
msgstr "A tároló b&eállításai..."

#: kwalletmanager.cpp:168
#, kde-format
msgid "Close &All Wallets"
msgstr "Az összes táro&ló bezárása"

#: kwalletmanager.cpp:257 walletcontrolwidget.cpp:111
#, kde-format
msgid ""
"Unable to close wallet cleanly. It is probably in use by other applications. "
"Do you wish to force it closed?"
msgstr ""
"Nem sikerült bezárni a tárolót, valószínűleg egy másik alkalmazás használja. "
"Mindenképpen be szeretné zárni?"

#: kwalletmanager.cpp:259 walletcontrolwidget.cpp:113
#, kde-format
msgid "Force Closure"
msgstr "Bezárás mindenképpen"

#: kwalletmanager.cpp:260 walletcontrolwidget.cpp:114
#, kde-format
msgid "Do Not Force"
msgstr "Nem kell bezárni"

#: kwalletmanager.cpp:264 walletcontrolwidget.cpp:118
#, kde-format
msgid "Unable to force the wallet closed. Error code was %1."
msgstr "A tárolót nem sikerült bezárni. A hibakód: %1."

#: kwalletmanager.cpp:280
#, kde-format
msgid "Error opening wallet %1."
msgstr "Hiba történt a következő jelszótároló megnyitásakor: %1."

#: kwalletmanager.cpp:322
#, kde-format
msgid "Please choose a name for the new wallet:"
msgstr "Adja meg az új tároló nevét:"

#: kwalletmanager.cpp:330
#, kde-format
msgctxt "@title:window"
msgid "New Wallet"
msgstr "Új tároló"

#: kwalletmanager.cpp:349
#, kde-format
msgid "Empty name is not supported. Please select a new one."
msgstr "Az üres név nem támogatott. Válasszon egy újat."

#: kwalletmanager.cpp:349
#, kde-format
msgid "Create new wallet"
msgstr "Új tároló létrehozása"

#: kwalletmanager.cpp:355
#, kde-format
msgid "Sorry, that wallet already exists. Try a new name?"
msgstr "Ilyen nevű tároló már létezik. Próbáljon más nevet megadni."

#: kwalletmanager.cpp:357
#, kde-format
msgid "Try New"
msgstr "Újabb próba"

#: kwalletmanager.cpp:383
#, kde-format
msgid "Are you sure you wish to delete the wallet '%1'?"
msgstr "Biztosan törölni szeretné ezt a jelszótárolót: \"%1\"?"

#: kwalletmanager.cpp:389
#, kde-format
msgid "Unable to delete the wallet. Error code was %1."
msgstr "A tárolót nem sikerült törölni. A hibakód: %1."

#: kwalletmanager.cpp:437
#, kde-format
msgid "File name"
msgstr "Fájlnév"

#: kwalletmanager.cpp:444
#, kde-format
msgid "Failed to open file for writing"
msgstr "Nem sikerült megnyitni a fájlt írásra"

#: kwalletmanager.cpp:455
#, kde-format
msgid "Select file"
msgstr "Fájl kiválasztása"

#: kwalletmanager.cpp:465
#, kde-format
msgid "Failed to open file"
msgstr "Nem sikerült megnyitni a fájlt"

#: kwalletmanager.cpp:478
#, kde-format
msgid "Wallet named %1 already exists, Operation aborted"
msgstr "Már létezik %1 nevű tároló. Művelet megszakítva."

#: kwalletmanager.cpp:485
#, kde-format
msgid "Failed to copy files"
msgstr "A fájlok másolása meghiúsult"

#: kwalletmanager.cpp:498 walletcontrolwidget.cpp:101
#, kde-format
msgid "Ignore unsaved changes?"
msgstr "Mellőzi a nem mentett változtatásokat?"

#: kwalletmanager.cpp:498 walletcontrolwidget.cpp:101
#, kde-format
msgid "Ignore"
msgstr "Mellőzés"

#. i18n: ectx: Menu (file)
#: kwalletmanager.rc:4
#, kde-format
msgid "&File"
msgstr "&Fájl"

#. i18n: ectx: Menu (settings)
#: kwalletmanager.rc:17
#, kde-format
msgid "&Settings"
msgstr "&Beállítások"

#. i18n: ectx: Menu (help)
#: kwalletmanager.rc:20
#, kde-format
msgid "&Help"
msgstr "&Súgó"

#: kwalletmanagerwidget.cpp:224
#, kde-kuit-format
msgctxt "@info:status"
msgid "An error occurred when connecting to the KWallet service:<nl/>%1"
msgstr "Hiba történt a KWallet szolgáltatáshoz csatlakozáskor:<nl/>%1"

#: kwalletpopup.cpp:32 walletcontrolwidget.cpp:77
#, kde-format
msgid "&Open..."
msgstr "Me&gnyitás..."

#: kwalletpopup.cpp:38
#, kde-format
msgid "Change &Password..."
msgstr "A jelszó meg&változtatása..."

#: kwalletpopup.cpp:51
#, kde-format
msgid "Disconnec&t"
msgstr "A kapcsolat &bontása"

#: kwmapeditor.cpp:129
#, kde-format
msgid "Key"
msgstr "Kulcs"

#: kwmapeditor.cpp:129
#, kde-format
msgid "Value"
msgstr "Érték"

#: kwmapeditor.cpp:147 kwmapeditor.cpp:196
#, kde-format
msgid "Delete Entry"
msgstr "Bejegyzés törlése"

#: kwmapeditor.cpp:216
#, kde-format
msgid "&New Entry"
msgstr "Ú&j bejegyzés"

#: main.cpp:27
#, kde-format
msgid "Wallet Manager"
msgstr "Jelszókezelő"

#: main.cpp:29
#, kde-format
msgid "KDE Wallet Management Tool"
msgstr "KDE-alapú program jelszavak biztonságos kezeléséhez"

#: main.cpp:31
#, kde-format
msgid "Copyright ©2013–2017, KWallet Manager authors"
msgstr "Copyright © A KWallet szerzői, 2013-2017."

#: main.cpp:35
#, kde-format
msgid "Valentin Rusu"
msgstr "Valentin Rusu"

#: main.cpp:36
#, kde-format
msgid "Former Maintainer, user interface refactoring"
msgstr "Korábbi karbantartó, felhasználói felület újraírása"

#: main.cpp:38
#, kde-format
msgid "George Staikos"
msgstr "George Staikos"

#: main.cpp:39
#, kde-format
msgid "Original author and former maintainer"
msgstr "Az eredeti szerző, korábbi karbantartó"

#: main.cpp:41
#, kde-format
msgid "Michael Leupold"
msgstr "Michael Leupold"

#: main.cpp:42
#, kde-format
msgid "Developer and former maintainer"
msgstr "Fejlesztő és korábbi karbantartó"

#: main.cpp:44
#, kde-format
msgid "Isaac Clerencia"
msgstr "Isaac Clerencia"

#: main.cpp:45
#, kde-format
msgid "Developer"
msgstr "Fejlesztő"

#: main.cpp:53
#, kde-format
msgid "Show window on startup"
msgstr "Az ablak jelenjen meg indításkor"

#: main.cpp:54
#, kde-format
msgid "For use by kwalletd only"
msgstr "Csak a kwalletd használhatja"

#: main.cpp:55
#, kde-format
msgid "A wallet name"
msgstr "A tároló neve"

#: revokeauthbutton.cpp:16
#, kde-format
msgid "Revoke Authorization"
msgstr "Felhatalmazás visszavonása"

#: walletcontrolwidget.cpp:57
#, kde-format
msgid "&Close"
msgstr "&Bezárás"

#: walletcontrolwidget.cpp:74
#, kde-format
msgctxt ""
"the 'kdewallet' is currently open (e.g. %1 will be replaced with current "
"wallet name)"
msgid "The '%1' wallet is currently open."
msgstr "A(z) „%1” tároló jelenleg nyitva van."

#: walletcontrolwidget.cpp:91
#, kde-format
msgid "The wallet is currently closed."
msgstr "A tároló jelenleg zárva van."

#. i18n: ectx: property (text), widget (KSqueezedTextLabel, _stateLabel)
#: walletcontrolwidget.ui:31
#, kde-format
msgid "KSqueezedTextLabel"
msgstr "KSqueezedTextLabel"

#. i18n: ectx: property (text), widget (QPushButton, _openClose)
#: walletcontrolwidget.ui:54
#, kde-format
msgid "Open..."
msgstr "Megnyitás…"

#. i18n: ectx: property (text), widget (QPushButton, _changePassword)
#: walletcontrolwidget.ui:61
#, kde-format
msgid "Change Password..."
msgstr "A jelszó megváltoztatása…"

#. i18n: ectx: attribute (title), widget (QWidget, _contentsTab)
#: walletcontrolwidget.ui:80
#, kde-format
msgid "Contents"
msgstr "Tartalom"

#. i18n: ectx: attribute (title), widget (QWidget, _appsTab)
#: walletcontrolwidget.ui:117
#, kde-format
msgid "Applications"
msgstr "Alkalmazások"

#. i18n: ectx: property (text), widget (QToolButton, _hideContents)
#: walletwidget.ui:159
#, kde-format
msgid "Hide &Contents"
msgstr "A tartalom el&rejtése"

#. i18n: ectx: property (text), widget (QCheckBox, _binaryViewShow)
#. i18n: ectx: property (text), widget (QToolButton, _showContents)
#: walletwidget.ui:205 walletwidget.ui:253
#, kde-format
msgid "Show &Contents"
msgstr "A tartalom megje&lenítése"

#. i18n: ectx: property (text), widget (QPushButton, _undoChanges)
#: walletwidget.ui:324
#, kde-format
msgid "&Undo"
msgstr "&Visszavonás"

#. i18n: ectx: property (text), widget (QPushButton, _saveChanges)
#: walletwidget.ui:334
#, kde-format
msgid "&Save"
msgstr "Men&tés"

#~ msgid "Open Wallet..."
#~ msgstr "Jelszótároló megnyitása…"

#~ msgid "Error opening XML file '<b>%1</b>' for input."
#~ msgstr "Nem sikerült megnyitni olvasásra a(z) '<b>%1</b>' XML-fájlt."

#~ msgid ""
#~ "This is a binary data entry.  It cannot be edited as its format is "
#~ "unknown and application specific."
#~ msgstr ""
#~ "Ez egy bináris adatokat tartalmazó bejegyzés. A tartalma nem "
#~ "szerkeszthető, mert a formátuma nem ismert (az alkalmazástól függ)."

#~ msgid "(c) 2003,2004 George Staikos"
#~ msgstr "(C) George Staikos, 2003, 2004."

#~ msgid "Maintainer"
#~ msgstr "Karbantartó"

#~ msgid "&Export..."
#~ msgstr "E&xportálás..."

#, fuzzy
#~| msgid "Change &Password..."
#~ msgid "Change password..."
#~ msgstr "A jelszó meg&változtatása..."

#~ msgid "That wallet file already exists.  You cannot overwrite wallets."
#~ msgstr "A jelszótároló fájl már létezik. Tárolókat nem lehet felülírni!"

#~ msgid ""
#~ "This wallet was forced closed.  You must reopen it to continue working "
#~ "with it."
#~ msgstr ""
#~ "A tároló be lett zárva. Ha továbbra is használni szeretné, nyissa meg "
#~ "újból."
